#!/bin/bash

echo "Buliding busybox"
echo "Uncompress the busybox  tarball and change into its directory. Then load the default compilation configuration template:"
echo " Extract file for error free operation"
echo "cd busybox-xxx directory "

make CROSS_COMPILE="${CLOS_TARGET}-" defconfig

echo "The default configuration template will enable the compilation of a default defined set of utilities and libraries. You can enable/disable whatever you see fit by running menuconfig:"


make CROSS_COMPILE="${CLOS_TARGET}-" menuconfig

echo "Compile and install the package:"


make CROSS_COMPILE="${CLOS_TARGET}-"
make CROSS_COMPILE="${CLOS_TARGET}-" \
CONFIG_PREFIX="${CLOS}" install

echo "Install the following Perl script, as you'll need it for the kernel build below:"


cp -v examples/depmod.pl ${CLOS}/cross-tools/bin
chmod 755 ${CLOS}/cross-tools/bin/depmod.pl
