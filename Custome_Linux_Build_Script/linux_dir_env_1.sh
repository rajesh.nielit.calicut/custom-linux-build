#!/bin/bash
mkdir -pv ${CLOS}
mkdir -pv ${CLOS}/{bin,boot{,grub},dev,{etc/,}opt,home,lib/{firmware,modules},lib64,mnt}
mkdir -pv ${CLOS}/{proc,media/{floppy,cdrom},sbin,srv,sys}
mkdir -pv ${CLOS}/var/{lock,log,mail,run,spool}
mkdir -pv ${CLOS}/var/{opt,cache,lib/{misc,locate},local}
install -dv -m 0750 ${CLOS}/root
install -dv -m 1777 ${CLOS}{/var,}/tmp
install -dv ${CLOS}/etc/init.d
mkdir -pv ${CLOS}/usr/{,local/}{bin,include,lib{,64},sbin,src}
mkdir -pv ${CLOS}/usr/{,local/}share/{doc,info,locale,man}
mkdir -pv ${CLOS}/usr/{,local/}share/{misc,terminfo,zoneinfo}
mkdir -pv ${CLOS}/usr/{,local/}share/man/man{1,2,3,4,5,6,7,8}
for dir in ${CLOS}/usr{,/local}; do
     ln -sv share/{man,doc,info} ${dir}
   done
