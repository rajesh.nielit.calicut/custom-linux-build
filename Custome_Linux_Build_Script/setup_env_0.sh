#!/bin/bash
set +h
umask 022
export CLOS=~/cl-os
export LC_ALL=POSIX
export PATH=${CLOS}/cross-tools/bin:/bin:/usr/bin
unset CFLAGS
unset CXXFLAGS
export CLOS_HOST=$(echo ${MACHTYPE} | sed "s/-[^-]*/-cross/")
export CLOS_TARGET=x86_64-unknown-linux-gnu
export CLOS_CPU=k8
export CLOS_ARCH=$(echo ${CLOS_TARGET} | sed -e 's/-.*//' -e 's/i.86/i386/')
export CLOS_ENDIAN=little
export CC="${CLOS_TARGET}-gcc"
export CXX="${CLOS_TARGET}-g++"
export CPP="${CLOS_TARGET}-gcc -E"
export AR="${CLOS_TARGET}-ar"
export AS="${CLOS_TARGET}-as"
export LD="${CLOS_TARGET}-ld"
export RANLIB="${CLOS_TARGET}-ranlib"
export READELF="${CLOS_TARGET}-readelf"
export STRIP="${CLOS_TARGET}-strip"
