echo "The Linux Kernel"

echo "Change directory  into the kernel package directory and run the following to set the default x86-64 configuration template:"


make ARCH=${CLOS_ARCH} \
CROSS_COMPILE=${CLOS_TARGET}- x86_64_defconfig

echo "This will define a minimum set of modules and settings for the compilation process. You most likely will need to make the proper adjustments for the target machine's environment. This includes enabling modules for storage and networking controllers and more. You can do that with the menuconfig option:"
echo "diable networking , security etc. for building minimal kernel"

make ARCH=${CLOS_ARCH} \
CROSS_COMPILE=${CLOS_TARGET}- menuconfig


echo "Compile and install the kernel:"


make ARCH=${CLOS_ARCH} \
CROSS_COMPILE=${CLOS_TARGET}-
make ARCH=${CLOS_ARCH} \
CROSS_COMPILE=${CLOS_TARGET}- \
INSTALL_MOD_PATH=${CLOS} modules_install

echo "You'll need to copy a few files into the /boot directory for GRUB:"


cp -v arch/x86/boot/bzImage ${CLOS}/boot/vmlinuz-4.16.3
cp -v System.map ${CLOS}/boot/System.map-4.16.3
cp -v .config ${CLOS}/boot/config-4.16.3

echo "Then running the previously installed Perl script provided by the BusyBox package:"


${CLOS}/cross-tools/bin/depmod.pl \
-F ${CLOS}/boot/System.map-4.16.3 \
-b ${CLOS}/lib/modules/4.16.3

echo "The Bootscripts"

echo "The Cross Linux From Scratch (CLFS) project (a fork of the original LFS project) provides a wonderful set of bootscripts that I use here for simplicity's sake. Uncompress the package and change into its directory. Out of box, one of the package's makefiles contains a line that may not be compatible with your current working shell. Apply the following changes to the package's root Makefile to ensure that you don't experience any issues with package installation:"

echo "apply the patch"
echo "bootscripts.patch"

echo "Then run the following commands to install and configure the target environment appropriately:"


make DESTDIR=${CLOS}/ install-bootscripts
ln -sv ../rc.d/startup ${CLOS}/etc/init.d/rcS

echo "Optional Zlib"

echo " Zlib isn't a requirement, but it serves as a great guide for other packages you may want to install for your environment. Feel free to skip this step if you'd rather format and configure the physical or virtual HDD.

echo "Uncompress the Zlib tarball and change into its directory. Then configure, build and install the package:"


sed -i 's/-O3/-Os/g' configure
./configure --prefix=/usr --shared
make && make DESTDIR=${CLOS}/ install

echo "Now, because some packages may look for Zlib libraries in the /lib directory instead of the /lib64 directory, apply the following changes:"


mv -v ${CLOS}/usr/lib/libz.so.* ${CLOS}/lib
ln -svf ../../lib/libz.so.1 ${CLOS}/usr/lib/libz.so
ln -svf ../../lib/libz.so.1 ${CLOS}/usr/lib/libz.so.1
ln -svf ../lib/libz.so.1 ${CLOS}/lib64/libz.so.1
